import Vue from 'vue'
import IView from 'iview';
import 'iview/dist/styles/iview.css';
import App from './App.vue'
import store from './store'

Vue.config.productionTip = process.env.NODE_ENV !== 'production';

Vue.use(IView);

new Vue({
  store,
  render: h => h(App)
}).$mount('#app');
