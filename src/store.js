import Vue from 'vue';
import Vuex from 'vuex';
import jobs from './assets/jobs.json';

Vue.use(Vuex);

const CHANGE_PREFERENCE = 'CHANGE_PREFERENCE';

export default new Vuex.Store({
  state: {
    jobs,
    preferences: {
      "oursky-tw:software-engineer": null,
      "t1:frontend-developer": null
    },
  },
  mutations: {
    [CHANGE_PREFERENCE](state, { id, preference }) {
      Vue.set(state.preferences, id, preference);
    },
  },
  actions: {
    updatePreference({ commit }, { id, preference }) {
      commit(CHANGE_PREFERENCE, { id, preference })
    },
  }
})
